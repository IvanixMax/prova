Pasos a seguir para ejecutar el proyecto:

## Create docker network
docker network create prova-network --subnet 172.23.0.0/16


## < APACHE
## BUILD
docker build --tag prova .
## RUN
docker run --detach --net prova-network --rm --ip 172.23.0.2 -p 80:80 --name prova -v $PWD:/var/www/prova/ prova
## APACHE />


## MYSQL
docker run --detach --net prova-network --rm --ip 172.23.0.3 -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7

Para conectarse a la Base de datos:

ip: 127.23.0.3

usuario: root

password: root


Crear base de datos prova y importar de db/structure.sql en el contenedor MYSQL