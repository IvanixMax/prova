<?php

class Input
{
    private $db;

    /**
     * Input constructor.
     */
    public function __construct()
    {
        $this->db = new mysqli("172.23.0.3", "root", "root", "prova");

        if ($this->db->connect_errno) {
            echo "Failed to connect to MySQL: " . $this->db->connect_error;
            exit();
        }
    }

    /**
     * @return bool|mysqli_result
     */
    private function setNames()
    {
        return $this->db->query("SET NAMES 'utf8'");
    }

    /**
     * @param $string
     * @return string
     */
    public function toUppercase($string)
    {
        return strtoupper($string);
    }

    /**
     * @param $inputString
     * @return bool|string
     */
    public function setInput($inputString)
    {
        self::setNames();

        $upperCaseString = $this->toUppercase($inputString);

        $sql = "INSERT INTO inputs(input_text, throw_text) VALUES ('" . $inputString . "', '" . $upperCaseString . "')";
        $result = $this->db->query($sql);

        if ($result) {
            return $upperCaseString;
        } else {
            return false;
        }
    }
}

