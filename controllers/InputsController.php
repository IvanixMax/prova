<?php
require_once("./models/Input.php");

if( isset($_POST['inputString']) )
{
    $response = [];
    $inputString = $_POST['inputString'];
    $inputs = new Input();
    $stringUpperCase = $inputs->setInput($inputString);
    if($stringUpperCase !== false){
        $response['status'] = 'success';
        $response['data'] = $stringUpperCase;
    }else{
        $response['status'] = 'error';
        $response['error'] = [
            'error' => 'Unable to save values to the database.'
        ];
    }

    echo json_encode($response);
}else{
    http_response_code(422);
    die('No data passed.');
}
