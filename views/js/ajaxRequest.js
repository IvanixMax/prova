$(document).ready(function () {
    $("#transformString").click(function () {
        let string = $("#stringToUpper").val();
        let responseString = $("#responseString");

        if (string.length <= 0) {
            responseString.text('Debes añadir un string para poder transformarlo.');
            responseString.show();
            responseString.css('color', 'red');
        } else {


            var request = $.ajax({
                url: '/inputs',
                method: 'POST',
                data: {
                    inputString: string
                },
                dataType: "json"
            });

            request.done(function (response) {
                responseString.text('Respuesta: ' + response.data);
                responseString.show();
                responseString.css('color', 'green');
            });

            request.fail(function (jqXHR, response) {
                responseString.text('Hubo un error: ' + response.error);
                responseString.show();
                responseString.css('color', 'red');
            });
        }
    });
});
