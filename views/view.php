<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MVC</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="/views/css/app.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="/views/js/ajaxRequest.js"></script>

</head>
<body>
<div class="container">
    <header class="text-center">
        <h1>Ejemplo MVC con PHP</h1>
        <hr/>
    </header>
    <div class="row">
        <div class="col text-center mb-3">
            <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
                Pasar string a mayusculas
            </button>
        </div>

        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h4 class="modal-title">Pasar a mayusculas</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Entra tu string (255 carácteres máximo)</label>
                            <input type="text" class="form-control" id="stringToUpper"
                                   aria-describedby="stringToParse" name="inputString" placeholder="Entrar cadena"
                                   required>
                            <div class="col-12 mt-2">
                                <span id="responseString"></span>
                            </div>
                        </div>
                        <button type="button" id="transformString" class="btn btn-primary">Transformar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>