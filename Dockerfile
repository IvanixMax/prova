FROM ubuntu:18.04

ENV TZ=Europe/Madrid
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN \
    apt-get update && \
    apt-get install -y nano readline-common net-tools telnet lynx unzip iputils-ping && \
    apt-get install -y composer && \
    apt-get install -y php php-json php-dom php-xml php-mbstring php-curl php-zip php-intl php-mysql php-xdebug curl && \
    :

RUN \
    apt-get update && \
    apt-get install -y nodejs npm && \
    npm install -g yarn && \
    :

# Setup apache2 virtualhost.
COPY dockerconfig/provaVirtualhost.test.conf /etc/apache2/sites-available/prova.test.conf

RUN mkdir /var/www/prova

RUN \
    cd /etc/apache2/sites-available && \
    a2dissite 000-default.conf && \
    a2ensite prova.test.conf && \
    service apache2 restart && \
    :

# Setup color prompts for root and the user.
RUN \
    cd /root && \
    sed -i "s/#force_color_prompt=yes/force_color_prompt=yes/" .bashrc && \
    sed -i '53c\    PS1='"'"'${debian_chroot:+($debian_chroot)}\\u@\\[\\033[01;33m\\]\\h\\[\\033[00m\\]:\\[\\033[01;34m\\]\\w\\[\\033[00m\\]\\$ '"'" .bashrc && \
    :

# Expose apache and ssl. Also make the bin/console server:run to be accessible from the outside.
EXPOSE 80 443

# Start apache
CMD [ "apachectl", "-D", "FOREGROUND" ]
